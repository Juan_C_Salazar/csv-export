import pandas as pd
from tkinter import *
from tkinter.filedialog import askopenfile
from tkinter import messagebox
import os
import sqlite3
from datetime import *
import time

root = Tk()
date = time.strftime("%Y-%m-%d")
today = datetime.now()
delta = timedelta(days=1)
yesterday = datetime.strftime(today - delta, '%Y/%m/%d')
tomorrow = datetime.strftime(today + delta, '%Y/%m/%d')

executable_path = os.getcwd()
main_path = r'C:/Users/juank/Desktop/Suriname/' + date
if not os.path.exists(main_path):

    os.makedirs(main_path)
    file = askopenfile(title='Select the file')
    path = file.name
    query = "SELECT * FROM Characteristics;"

    def read_table_sqlite(path, sql):
        conn = sqlite3.connect(path)
        df = pd.read_sql_query(sql, conn)
        conn.close()
        return df

    nodes = read_table_sqlite(path, query)
    nodes['num'] = nodes['node_id'].astype(int)

    number_variables = nodes.filter(like='name_var').count(axis=1).head(1)
    list2 = list(range(1, number_variables[0] + 1))

    for row, i in nodes.iterrows():
        query1 = "select * from Node_%s WHERE date >='%s' AND date <='%s';" %(i['node_id'],yesterday,tomorrow)
        data = read_table_sqlite(path, query1)
        list = []
        date_serie = data['date']
        for j in date_serie:
            date1 = datetime.strptime(j, '%Y/%m/%d %H:%M:%S')
            date2 = timedelta(hours=3)
            res = date1 - date2
            list.append(res)
        data['new_date'] = list
        data['new_date'] = data['new_date'].astype(str)
        data = data.loc[data['new_date'].str.contains(date)].reset_index(drop=True)
        data.drop('date', axis=1, inplace=True)
        col_rw = [0] * number_variables[0]
        col_cl = [0] * number_variables[0]
        for n in list2:
            col_rw[n-1] = 's' + str(n) + '_' + i['name_var_%s'%n][0:3].lower() + '_rw[counts]'
            col_cl[n-1] = 's' + str(n) + '_' + i['name_var_%s'%n][0:3].lower() + '_cl[' + i['unit_%s'%n] + ']'
            data.rename(columns={'raw_%s'%n: col_rw[n-1],'physic_%s'%n: col_cl[n-1], 'new_date' : 'Date&Time'}, inplace=True)

        data.to_csv(main_path + r'/Node_' + i['node_id'] + '.csv', sep=',', encoding='utf-8')
else:
    messagebox.showinfo("Information", "The folder already exists")
